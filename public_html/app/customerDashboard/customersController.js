
(function() {
//    customers details
    var CustomersController = function($scope,$log, custFactory, appSettings) {
$scope.customers=[];
$scope.appSettings=appSettings;
function init()
{
   custFactory.getCustomers()
           .success(function(customers){
               $scope.customers=customers;
               
   })
   .error(function(data, status, header, config){
       $log.log(data.error+' '+status);
   });
}
init();
        
        $scope.deleteCustomer=function(custId){
            custFactory.deleteCustomer(custId)
                    .success(function(status){
                        if(status){
                            for(var i=0,len=$scope.customers.length;i<len;i++)
                    {
                        if($scope.customers[i].id==custId){
                            $scope.customers.splice(i,1);
                            break;
                        }
                    }
                        }
                        else
                        {
                            window.alert('Unable to delete customer');
                        }
            })
                    .error(function(data,status,headers,config){
                        
            });
        }
    };
    CustomersController.$inject = ['$scope','$log','custFactory','appSettings'];
//    adding controller to module
    angular.module('customersApp').controller('CustomersController', CustomersController);

}());