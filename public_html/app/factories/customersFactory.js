(function(){
    
    var custFactory=function($http)
    {
        console.log('customers');
        
        var factory={};
        factory.getCustomers=function(){
//            return customers;
              return $http.get('/customers');
        };
        factory.getCustomer=function(custId)
        {
            console.log('customers2');
//            for (var i = 0, len = customers.length; i < len; i++) {
//                
//                if (customers[i].id === custId)
//                {       console.log('customers1');           
//                    return customers[i];
//                }
//            }
//            return{};
                return $http.get('/customers/'+custId);
        }
        factory.getOrders=function()
        {
             console.log('custFac all orders');
             return $http.get('/orders')
        }
        factory.deleteCustomer=function(custId){
            return $http.delete('/customers/'+custId);
        }
        return factory;
    };
    
    custFactory.$inject=['$http'];
    angular.module('customersApp').factory('custFactory',custFactory);
}());
