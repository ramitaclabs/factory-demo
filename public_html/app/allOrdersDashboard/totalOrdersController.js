(function(){
  var TotalOrdersController=function($scope, custFactory){
      $scope.custorders=null;
      $scope.ordersTotal=0.0;
      $scope.totalType;
      
      function init()
      {
          custFactory.getOrders()
                  .success(function(orders){
                 $scope.orders=orders;
         console.log($scope.orders);
                 getOrdersTotal();
          })
                  .error(function(data,status,headers,config){
                      
          });
          
      }
      
      function getOrdersTotal()
      {
          var total=0;
          for(var i=0, len=$scope.orders.length ;i<len;i++)
          {
              total+=$scope.orders[i].total;
          }
          $scope.ordersTotal=total;
          $scope.totalType=($scope.ordersTotal>50)? 'success':'danger';
      }
      init();
  };
  TotalOrdersController.$inject=['$scope','custFactory']; 
  angular.module('customersApp').controller('TotalOrdersController',TotalOrdersController);
    
}());

