
(function() {
    var OrdersController = function($scope, $routeParams, custFactory) {
        var custId = $routeParams.custId;
        $scope.customer = null;

//getting order details of customer whose view orders is clicked
        function init()
        {
//            $scope.customer=custFactory.getCustomer(custId);
          custFactory.getCustomer(custId)
           .success(function(customer){
               
               $scope.customer=customer;
               
   })
   .error(function(data, status, header, config){
       //handle error
   });
        }

//Information of customers orders
       
        init();
    };

    OrdersController.$inject = ['$scope', '$routeParams','custFactory'];
    //    adding controller to module
    angular.module('customersApp').controller('OrdersController', OrdersController);

}());