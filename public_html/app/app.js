
(function() {
//    giving dependency of customersApp module on ngRoute module for routing
    var app = angular.module('customersApp', ['ngRoute']);
    
    app.config(function($routeProvider) {
        $routeProvider
                .when('/',
                        {
                            controller: 'CustomersController',
                            templateUrl: 'app/customerDashboard/customers.html'
                        })
                .when('/orders/:custId',
                        {
                            controller: 'OrdersController',
                            templateUrl: 'app/orders/orders.html'
                        })
                .when('/orders',
                        {
                            controller:'TotalOrdersController',
                            templateUrl:'app/allOrdersDashboard/totOrders.html'
            
                        })
                .otherwise({redirectTo: '/'});

    });

}());